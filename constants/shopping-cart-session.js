export default class Cart {
    constructor(cart) {
        this.items = cart.items || {};
        this.quantity = cart.quantity || 0;
    }
    add(id) {
        if (!this.items[id]) {    // san pham add chua add lan nao
            this.items[id] = 1;
        } else { // san pham nay da duoc add truoc do
            this.items[id]++;
        }
        this.quantity++;
    }
    remove(id) {
        this.quantity -= this.items[id];
        delete this.items[id];
    }
}
