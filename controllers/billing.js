import { lich_su_mua_hang } from '../models';
import { NGAY_DAT_HANG } from '../constants';
module.exports = {
    create(data) {
        return lich_su_mua_hang
        .create(data)
        .then(result => {
            return result;
        });
    },
    getBilling(condition) {
        return lich_su_mua_hang
        .findAll({
            where: condition,
            order: [[ NGAY_DAT_HANG, 'DESC']],
        })
        .then(results => results);
    },
    getDetailBilling(id) {
        return lich_su_mua_hang
        .findById(id)
        .then(result => result);
    },
    getBillingAdmin() {
        return lich_su_mua_hang
        .aggregate('account_id', 'count', {
            plain: false,
            attributes: ['account_id'],
            group: ['account_id']
        })
        .then(result => result);
    },
    updateBilling(data, condition) {
        return lich_su_mua_hang
        .update({
            status: data
        }, {
            where: {
                id: condition
            }
        })
        .then(results => results);
    }
}
