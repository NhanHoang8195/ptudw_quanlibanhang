import { account } from '../models';

module.exports = {
    createAccount(username, password) {
        return account
        .create(Object.assign({}, { username:username }, {password: password}, {admin: false}))
        .then(result => {
            return result;
        })
    },
    loginAccount(username) {
        return account
        .findById(username)
        .then(result => {
            return result;
        });
    },
    updateProfiles(data) {
        console.log(data);
        return account
        .update(data, {
            where: {
                username: data.username
            },
            returning: true
        })
        .then(result => {
            if (result[0] === 1) {
                return result[1][0];
            } else {
                return null;    // update failed
            }
        })
    }
}
