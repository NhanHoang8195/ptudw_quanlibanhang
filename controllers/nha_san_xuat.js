import { nha_san_xuat } from '../models';

module.exports = {
    getNhaSanXuat() {
        return nha_san_xuat
        .findAll()
        .then(results => results);
    },
    upsertData(data) {
        return nha_san_xuat
        .upsert(data, {
            fields: ['nha_san_xuat'],
            returning: true
        })
        .then(result => result);
    }
}
