import { loai_san_pham } from '../models';

module.exports = {
    getLoaiSanPham() {
        return loai_san_pham
        .findAll()
        .then(results => results);
    },
    upsertData(data) {
        return loai_san_pham
        .upsert(data, {
            fields: ['loai'],
            returning: true
        })
        .then(result => result);
    }
}
