import accountController from './account';
import billingController from './billing';
import productController from './product';
import loaiSanPhamController from './loai_san_pham';
import nhaSanXuatController from './nha_san_xuat';
module.exports = {
    accountController,
    billingController,
    loaiSanPhamController,
    nhaSanXuatController,
    productController
}
