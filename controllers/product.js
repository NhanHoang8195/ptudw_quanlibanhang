import { san_pham } from '../models';
import moment from 'moment';
module.exports = {
    createProduct(data) {
        return san_pham
        .create(data)
        .then(result => result);
    },
    createProducts(product) {
        product.map(onRow=> {
            onRow.ngay_nhap = moment(new Date()).format('YYYY-MM-DD HH:mm:ssZ')
        })
        return san_pham
        .bulkCreate(product)
        .then(results => {
            return results;
        });
    },
    getProductsFollowingCondition(condition = {}, additionalCondition = []) {
       return san_pham
       .findAll({
           where: condition,
           order: additionalCondition.order,
           limit: additionalCondition.limit,
           include: additionalCondition.include
       })
       .then(result =>{
           return result;
       });
   },
   updateProducts(condition, data) {
       return san_pham
       .update(data, condition)
       .then(result => {
          return result[0];
       });
   },
   deleteProduct(id) {
       return san_pham
       .destroy({
           where: {
               id: id
           }
       })
       .then(result => result);
   }
}
