This project using Nodejs, Sequelize, Handlebars.
How to run this project:
- Make sure u have installed #Postgresql (create user with username= postgres, password=postgres. Recommend install PgAdmin3
to contact database with UI if u don't similar with terminal), #Nodejs.
Go to the directory /ptudw_quanlibanhang and open terminal
- Run the command: #"npm install -g sequelize-cli". (type: #sequelize on the terminal to make sure it was installed).
- Run the command: #"npm install"
- Run the command: #"sequelize db:create" (type yes if get warning).
- Run the command: #"sequelize db:migrate" (type yes if get warning).
- Run the sql file: #"psql -U postgres -d quanlibanhang -f quanlibanhang.sql" (input password: postgres if required).
- Run the command: #"npm start" and go to browser type: localhost:8080 and check the result of the project.
