import bcrypt from 'bcryptjs';
import passport from 'passport';
import axios from 'axios';
import { GOOGLE_URL_API } from '../constants';
const LocalStrategy = require('passport-local').Strategy;
import { accountController } from '../controllers';
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    accountController.loginAccount(user.username)
    .then(result => {
        if (result) {
            return done(null, result.get());
        } else {
            return done(null, null);
        }
    })
    .catch(error => {
        return done(error, null);
    })
});
passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, email, password, done) {
        const url =
        axios.post(GOOGLE_URL_API.concat(req.body['g-recaptcha-response'])). then(response => {
            console.log(response.data);
            if (response.data.success) {
                const saltRounds = 10;
                bcrypt.hash(password, saltRounds)
                .then(hash => {
                        accountController.createAccount(email, hash)
                    .then(result => {
                        if (result) {
                            return done(null, result);
                        } else {
                            return done(null, false, {message: 'That email is already taken'});
                        }
                    })
                    .catch(error => {
                        return done(null, false, {message: 'Can not create account'});
                    })
                })
                .catch(error => {
                    return done(null, false, {message: 'Can not create account'});
                });
            } else {
                return done(null, false);
            }
        })
    }
));

passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    },
    function(req, email, password, done) {
        accountController.loginAccount(email)
        .then(result => {
            if (result) {   // found the username
                bcrypt.compare(password, result.password)
                .then( isOk => {
                    if (!req.session.redirectTo && result.admin) {
                        req.session.redirectTo = '/admin';
                    }
                    if (isOk) {
                        return done (null, result);
                    } else {
                        return done (null, false);
                    }
                });
            } else {     // username not found
                return done (null, false);
            }
        })
        .catch(error => {
            return done(error);
        });
    }
));
