export function isLoggedIn(req, res, next) {
    req.session.redirectTo = req.originalUrl;
    if (req.isAuthenticated()) {
        if (req.session.redirectTo.includes('/admin') && !req.user.admin) {
            return res.send({error: 'You can not access this page'});
        }
        return next();
    }
    res.redirect('/login');
}
