'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('account', {
        username: {
            allowNull: false,
            primaryKey: true,
            type: Sequelize.STRING
        },
        password: {
            allowNull:false,
            type: Sequelize.STRING
        },
        first_name: Sequelize.STRING,
        last_name: Sequelize.STRING,
        age: Sequelize.INTEGER,
        address: Sequelize.STRING,
        admin: Sequelize.BOOLEAN
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('account');
  }
};
