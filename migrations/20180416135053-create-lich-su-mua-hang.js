'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('lich_su_mua_hang', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        ngay_dat_hang: Sequelize.DATE,
        total: Sequelize.STRING,
        data: Sequelize.STRING,
        delivery_to: Sequelize.STRING,
        status: Sequelize.STRING,
        account_id: {
            type: Sequelize.STRING,
            references: {
                model: 'account',
                key: 'username'
            },
            onDelete: 'CASCADE',
            allowNull: false
        }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('lich_su_mua_hang');
  }
};
