'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('san_pham', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    ten_san_pham: Sequelize.STRING,
    hinh_anh: Sequelize.STRING,
    gia_ban: Sequelize.INTEGER,
    so_luot_xem:Sequelize.INTEGER,
    so_luong_ban: Sequelize.INTEGER,
    mo_ta: Sequelize.STRING,
    xuat_xu: Sequelize.STRING,
    loai: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
            model: 'loai_san_pham',
            key: 'loai'
        },
        onUpdate: 'CASCADE'
    },
    ngay_nhap: Sequelize.DATE,
    nha_san_xuat: {
        type: Sequelize.STRING,
        allowNull: false,
        references: {
            model: 'nha_san_xuat',
            key: 'nha_san_xuat'
        },
        onUpdate: 'CASCADE'
    },
    so_luong: Sequelize.INTEGER,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('san_pham');
  }
};
