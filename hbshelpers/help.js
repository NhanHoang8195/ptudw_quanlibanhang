import hbs from 'hbs';
import { DA_GIAO_HANG } from '../constants';

const conditionIf = hbs.registerHelper('ifCondition', function (arg1, arg2, options){
    if (arg1 === arg2) {
        return options.fn(this);
    }
    return options.inverse(this);
});
const ifStatus = hbs.registerHelper('ifStatus', function (arg, options){
    console.log(arg);
    if (arg === DA_GIAO_HANG) {
        return options.fn(this);
    }
    return options.inverse(this);
});

module.exports = {
    conditionIf,
    ifStatus
}
