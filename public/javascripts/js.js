function addTypeOfProducts() {
    $('.type-products').append('<input type="text" class="form-control" placeholder="Type of product" name="loai" required />');
}
function addProducers() {
    $('.type-products').append('<input type="text" class="form-control" placeholder="Producer" name="nhasx" required />');
}
function confirmDelete(id) {
    var url = "/admin/products/delete/" + id;
    $('.modal-footer a').attr("href", url);
}
function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight)+"px";
}
function readURL(input) {
    console.log(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            document.getElementById('image-preview').src=e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function changeImage(event) {
    console.log('haha');
    readURL(event);
}
function overlayShow() {
    console.log('haha');
    $.LoadingOverlay("show");
}
function recaptchaCallback() {
    $('#submitRegister').removeAttr('disabled');
};
