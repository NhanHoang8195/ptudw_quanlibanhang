'use strict';
module.exports = (sequelize, DataTypes) => {
  var loai_san_pham = sequelize.define('loai_san_pham', {
    id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: DataTypes.INTEGER
    },
    loai: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    }
  }, {
      freezeTableName: true,
      underscored: true,
      timestamps: false
  });
  loai_san_pham.associate = models => {
    loai_san_pham.hasMany(models.san_pham, {
        foreignKey: 'loai',
        sourceKey: 'loai',
        onDelete: 'CASCADE'
    });
  };
  return loai_san_pham;
};
