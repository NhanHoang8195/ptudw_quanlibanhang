'use strict';
module.exports = (sequelize, DataTypes) => {
  var nha_san_xuat = sequelize.define('nha_san_xuat', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    nha_san_xuat: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    }
  }, {
      freezeTableName: true,
      underscored: true,
      timestamps: false
  });
  nha_san_xuat.associate = models => {
      nha_san_xuat.hasMany(models.san_pham, {
          foreignKey: 'nha_san_xuat',
          sourceKey: 'nha_san_xuat',
          onDelete: 'CASCADE'
      });
  };
  return nha_san_xuat;
};
