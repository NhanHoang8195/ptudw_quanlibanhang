'use strict';
module.exports = (sequelize, DataTypes) => {
  var lich_su_mua_hang = sequelize.define('lich_su_mua_hang', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    ngay_dat_hang: DataTypes.STRING,
    total: DataTypes.STRING,
    data: DataTypes.TEXT,
    delivery_to: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    freezeTableName: true,
    underscored: true,
    timestamps: false
  });
    lich_su_mua_hang.associate = models => {
        lich_su_mua_hang.belongsTo(models.account, {
            foreignKey: 'account_id',
            onDelete: 'CASCADE'
        });
  };
  return lich_su_mua_hang;
};
