'use strict';
module.exports = (sequelize, DataTypes) => {
  var san_pham = sequelize.define('san_pham', {
    id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
    },
    ten_san_pham: DataTypes.STRING,
    hinh_anh: DataTypes.STRING,
    gia_ban: DataTypes.INTEGER,
    so_luot_xem: DataTypes.INTEGER,
    so_luong_ban: DataTypes.INTEGER,
    mo_ta: DataTypes.STRING,
    xuat_xu: DataTypes.STRING,
    ngay_nhap: DataTypes.STRING,
    so_luong: DataTypes.INTEGER
  }, {
        freezeTableName: true,
        underscored: true,
        timestamps: false
  });
    san_pham.associate = models => {
        san_pham.belongsTo(models.loai_san_pham, {
            foreignKey: 'loai',
            as: 'loai_san_pham',
            targetKey: 'loai'
        });
        san_pham.belongsTo(models.nha_san_xuat, {
            foreignKey: 'nha_san_xuat',
            as: 'nha_sx',
            targetKey: 'nha_san_xuat'
    })};
  return san_pham;
};
