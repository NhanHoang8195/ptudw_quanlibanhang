'use strict';
module.exports = (sequelize, DataTypes) => {
  var account = sequelize.define('account', {
    username: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.STRING
    },
    password: {
        allowNull:false,
        type: DataTypes.STRING
    },
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    address: DataTypes.STRING,
    admin: DataTypes.BOOLEAN
  }, {
    freezeTableName: true,
    underscored: true,
    timestamps: false
  });
    account.associate = function(models) {
        account.hasMany(models.lich_su_mua_hang, {
            foreignKey: 'account_id',
        });
  };
  return account;
};
