import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import passport from 'passport';
import session from 'express-session';
import helper from './hbshelpers/help';

import index from './routes/index';
import detailRouter from './routes/details';
import assortmentRouter from './routes/assortment';
import producerRouter from './routes/producer';
import searchRouter from './routes/search';
import loginRouter from './routes/login';
import signupRouter from './routes/signup';
import accountRouter from './routes/account';
import passportConfig from './config/passport';
import logoutRouter from './routes/logout';
import addProducts from './routes/cart';
import billingRouter from './routes/billing';

import adminRouter from './routes/admin/index';
import adminProductsRouter from './routes/admin/product';
import adminAssortmentRouter from './routes/admin/assortments';
import adminProducersRouter from './routes/admin/producer';
import adminBillingRouter from './routes/admin/billings';

import { isLoggedIn } from './config/isLoggedIn';

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized:true
})); // session secret
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
    res.locals.session = req.session;
    next();
});

app.use('/', index);
app.use('/details', detailRouter);  // chi tiet san pham
app.use('/assortment', assortmentRouter);  // loai san pham
app.use('/producer', producerRouter);   // nha san xuat
app.use('/search', searchRouter);
app.use('/account', accountRouter);
app.use('/login', loginRouter);
app.use('/signup', signupRouter);
app.use('/logout', logoutRouter);
app.use('/cart', addProducts);
app.use('/billing', isLoggedIn , billingRouter);

app.use('/admin', isLoggedIn, adminRouter);
app.use('/admin/products', adminProductsRouter);
app.use('/admin/assortment', adminAssortmentRouter);
app.use('/admin/producer', adminProducersRouter);
app.use('/admin/billings', adminBillingRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
