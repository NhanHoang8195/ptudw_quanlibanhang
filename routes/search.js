import express from 'express';
import { productController } from '../controllers';
import { NAME, PRODUCER, ASSORTMENT, PRICE, MADE } from '../constants';
const { ne, iLike } = require('sequelize').Op;

const router = express.Router();

router.get('/', (req, res, next) => {
    const condition = {};
    if (req.query.critical === NAME) {
        condition.ten_san_pham = {
            [iLike]: `%${req.query.q}%`
        }
    }
    if (req.query.critical === PRODUCER) {
        condition.nha_san_xuat = {
            [iLike]: `%${req.query.q}%`
        }
    }
    if (req.query.critical === ASSORTMENT) {
        condition.loai = {
            [iLike]: `%${req.query.q}%`
        }
    }
    if (req.query.critical === PRICE) {
        condition.gia_ban = req.query.q
    }
    if (req.query.critical === MADE) {
        condition.xuat_xu = {
            [iLike]: `%${req.query.q}%`
        }
    }
    productController.getProductsFollowingCondition(condition)
    .then(results => {
        if (req.user && req.user.admin) {
            if (!results) {
                return res.render('admin/admin-products', {
                    title: '0 search results',
                    layout: 'admin/admin-layout'
                })
            }
            res.render('admin/admin-products', {
                title: results.length + ' search results',
                layout: 'admin/admin-layout',
                data: results
            })
        } else {
            if (!results) {
                return res.render('products', {
                    title: '0 search results'
                })
            }
            res.render('products', {
                title: results.length + ' search results',
                data: results
            })
        }
    })
    .catch(error => {
        console.log(error);
        res.status(500).send({error: error});
    });
})
export default router;
