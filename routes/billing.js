import express from 'express';
const { or } = require('sequelize').Op;
import moment from 'moment';
import { billingController, productController } from '../controllers';

let router = express.Router();
router.get('/', (req, res, next) => {
    const condition = {
        account_id: req.user.username
    };
    billingController.getBilling(condition)
    .then(results => {  // list bought
        if (results && results.length > 0) {
            results.map(result => {
                result.ngay_dat_hang =  moment(result.ngay_dat_hang).format('dddd, YYYY-MM-DD HH:mm:ss');
            })
            return res.render('billing', {
                data: results,
                title: 'Billings'
            })
        } else {
            return res.render('billing', {
                data: null,
                title: 'Billings'
            });
        }
    })
    .catch(error => {
        console.log(error);
        return res.send(JSON.stringify(error));
    })
});
router.get('/:id', (req, res, next) => {
    const id = req.params.id;
    billingController.getDetailBilling(id)
    .then(result => {
        if (result) {
            const data = JSON.parse(result.data)
            const ids = Object.keys(data);
            const condition = {
                id: {
                    [or]: ids
                }
            }
            result.ngay_dat_hang = moment(result.ngay_dat_hang).format('dddd, YYYY-MM-DD HH:mm:ss');
            productController.getProductsFollowingCondition(condition)
            .then(results => {
                results.map(eachResult => {
                    eachResult.quantity = data[eachResult.id];
                    console.log(eachResult.quantity);
                })
                return res.render('billingdetail', {
                    dataDetail: results,
                    title: 'Billing',
                    data: result,
                })
            })
            .catch(error => {
                console.log(error);
                return res.send(error)
            });
        } else {
            return res.render('billingdetail', {
                title: 'Billing',
                data: null
            });
        }
    })
    .catch(error => {
        console.log(error);
        return res.send(error)
    })
});
export default router;
