import express from 'express';
import { productController } from '../controllers';
const { ne } = require('sequelize').Op;

const router = express.Router();

router.get('/:id', (req, res, next) => {
    const conditionDetails = {
        id: req.params.id
    }
    productController.getProductsFollowingCondition(conditionDetails)
    .then(results => {   // tat ca cac san pham dien thoai
        if (results.length === 0) {
            return res.render('detail', {
                title: 'Can not find product with id = ' + req.params.id
            });
        }
        results[0].so_luot_xem += 1;
        productController.updateProducts({where: {id: req.params.id}}, {so_luot_xem: results[0].so_luot_xem})
        .then(result => result)
        .catch(error => error);
        let conditionProductsType = {
            loai: results[0].loai,
            id: {
                [ne]: req.params.id
            }
        }
        productController.getProductsFollowingCondition(conditionProductsType, {limit: 5})
        .then(resultsProductsType => {
            conditionProductsType = {
                nha_san_xuat: results[0].nha_san_xuat,
                id: {
                    [ne]: req.params.id
                }
            }
            productController.getProductsFollowingCondition(conditionProductsType, {limit: 5})
            .then(resultsProductProducer => {
                res.render('detail', {
                    title: results[0].ten_san_pham,
                    data: results[0],
                    dataAssortment: resultsProductsType,
                    dataProducer: resultsProductProducer
                });
            })
        })
    })
    .catch(error => {
        console.log(error);
        res.status(500).send({error: error});
    });
})
export default router;
