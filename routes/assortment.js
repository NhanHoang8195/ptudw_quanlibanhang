import express from 'express';
import { productController, loaiSanPhamController } from '../controllers';

const router = express.Router();

router.get('/', (req, res, next) => {
    if (req.query.typeproducts) {
        productController.getProductsFollowingCondition({loai: req.query.typeproducts})
        .then(results => {
            return res.render('products', {
                    title: 'Danh sách các sản phẩm của ' + req.query.typeproducts,
                    data: results
                });
        })
        .catch(error => {
            return res.send(error);
        });
    } else {
        loaiSanPhamController.getLoaiSanPham()
        .then(results => {
            return res.render('assortment', {
                    title: 'Danh sách các loại mặt hàng',
                    data: results
                })
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({error: error});
        });
    }
});

export default router;
