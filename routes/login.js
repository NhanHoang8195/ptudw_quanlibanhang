import express from 'express';
import passport from 'passport';
const router = express.Router();

router.get('/', (req, res, next) => {
    res.render('login', { title: 'Login', layout: false});
});

router.post('/', passport.authenticate('local-login', {
    failureRedirect: '/login'
}), (req, res) => {
    res.redirect(req.session.redirectTo || '/');
    delete req.session.redirectTo;
});
export default router;
