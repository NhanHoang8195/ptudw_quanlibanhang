import express from 'express';
import passport from 'passport';
const router = express.Router();

router.get('/', (req, res, next) => {
    res.render('register', { title: 'Sigin', layout: false});
});
router.post('/', passport.authenticate('local-signup', {
    successRedirect: '/',
    failureRedirect: '/signup'
}));
export default router;
