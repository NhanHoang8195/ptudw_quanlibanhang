'use strict';
import express from 'express';
import passport from 'passport';
import { accountController } from '../controllers';

const router = express.Router();

router.post('/profiles', (req, res, next) => {
    accountController.updateProfiles(req.body)
    .then(data => {
        if (data) { // update successful
            if (req.session.previousUrl) {
                const previousUrl = req.session.previousUrl;
                delete req.session.previousUrl;
                return res.redirect(previousUrl);
            } else {
                return res.render('profiles', {
                    title: 'Profiles',
                    data: data,
                    flashmessage: 'Update profile successful!!!'
                });
            }
        } else {    // update failed
            return res.render('profiles', {
                title: 'Profiles'
            });
        }
    })
    .catch(error => {
        return res.send(error);
    });
});
router.get('/profiles', (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    return res.render('profiles', {
        title: 'Profiles',
        data: req.user
    });
});
module.exports = router;
