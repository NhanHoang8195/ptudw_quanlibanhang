import express from 'express';
import { productController } from '../../controllers';
const { ne } = require('sequelize').Op;

const router = express.Router();

router.get('/', (req, res, next) => {
    const conditionDetails = {
        id: req.query.id
    }
    productController.getProductsFollowingCondition(conditionDetails)
    .then(results => {   // tat ca cac san pham dien thoai
        if (results.length === 0) {
            return res.render('admin/admin-detail', {
                title: 'Can not find product with id = ' + req.query.id
            });
        } else {
            return res.render('admin/admin-detail', {
                title: 'Detail',
                data: results
            });
        }
    })
    .catch(error => {
        console.log(error);
        res.status(500).send({error: error});
    });
})
export default router;
