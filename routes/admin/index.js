'use strict';
import express from 'express';
import { productController } from '../../controllers';

let router = express.Router();

router.get('/', (req, res, next) => {
    return res.render('admin/admin-index', {
        title: 'Dashboard',
        layout: 'admin/admin-layout.hbs'
    });
});
export default router;
