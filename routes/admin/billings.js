import express from 'express';
import sleep from 'system-sleep';
const { or } = require('sequelize').Op;
import moment from 'moment';
import { CHUA_GIAO_HANG, DANG_GIAO_HANG, DA_GIAO_HANG } from '../../constants';
import { billingController } from '../../controllers';

let router = express.Router();
router.get('/', (req, res, next) => {
    billingController.getBillingAdmin()
    .then(results => {  // list bought
        if (results && results.length > 0) {
            return res.render('admin/admin-billing', {
                data: results,
                layout: 'admin/admin-layout',
                title: 'List billing'
            })
        } else {
            return res.render('admin/admin-billing', {
                data: null,
                layout: 'admin/admin-layout',
                title: 'No billing'
            });
        }
    })
    .catch(error => {
        console.log(error);
        return res.send(JSON.stringify(error));
    })
});
router.get('/:username', (req, res, next) => {
    const condition = {
        account_id: req.params.username
    }
    billingController.getBilling(condition)
    .then(results => {
        if (results && results.length > 0) {
            results.map(result => {
                result.ngay_dat_hang =  moment(result.ngay_dat_hang).format('dddd, YYYY-MM-DD HH:mm:ss');
                result.dataOption = [ CHUA_GIAO_HANG, DANG_GIAO_HANG, DA_GIAO_HANG ];
            })
            res.render('admin/admin-billing-detail', {
                data: results,
                layout: 'admin/admin-layout',
                flashMessage: req.session.flashMessage,
                title: req.params.username
            })
            delete req.session.flashMessage;
        } else {
            return res.render('admin/admin-billing-detail', {
                layout: 'admin/admin-layout',
                title: 'No billing'
            });
        }
    })
    .catch(error => {
        console.log(error);
        return res.send(error)
    })
});
router.post('/:username', (req, res, next) => {
    if (Array.isArray(req.body.id)) {
        let index = 0;
        for (let i = 0; i < req.body.status.length; i++) {
            if (req.body.status[i] === CHUA_GIAO_HANG) {
                billingController.updateBilling(CHUA_GIAO_HANG, req.body.id[i])
            } else if (req.body.status[i] === DANG_GIAO_HANG){
                billingController.updateBilling(DANG_GIAO_HANG, req.body.id[i])
            } else {
                billingController.updateBilling(DA_GIAO_HANG, req.body.id[i])
            }
        }
        sleep(500);
        return res.redirect(req.originalUrl);
    } else {
        billingController.updateBilling(req.body.status, req.body.id)
        .then(result => result).then(result => {
            return res.redirect(req.originalUrl);
        });
    }
});
export default router;
