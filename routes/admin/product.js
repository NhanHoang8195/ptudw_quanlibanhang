import express from 'express';
var multer = require('multer');
import moment from 'moment';
import { productController, loaiSanPhamController, nhaSanXuatController } from '../../controllers';
import { nha_san_xuat, loai_san_pham, san_pham } from '../../models';
import { BASE_URL_IMG } from '../../constants';
const upload = multer({ dest: 'public/images/mayanhso/' })


const router = express.Router();

router.post('/', (req, res, next) => {
    productController.createProducts(req.body)
    .then(results => {   // tat ca cac san pham dien thoai
        return res.status(201).send(results);
    })
    .catch(error => {
        console.log(error);
        res.status(500).send({error: error});
    });
});
router.get('/delete/:id', (req, res, next) => {
    productController.deleteProduct(req.params.id)
    .then(result =>{
        if (result === 1) {
            req.session.flashMessage = 'Delete product successfully';
            return res.redirect('/admin/products');
        } else {
            throw new Error('Can not delete this product');
        }
    })
    .catch(error => res.status(500).send(error.toString()));
});
router.get('/edit/:id', (req, res, next) => {
    const condition = {
        id: req.params.id,
    };
    productController.getProductsFollowingCondition(condition)
    .then(results => {
        loaiSanPhamController.getLoaiSanPham()
        .then(resultsLoaiSanPham => {
            nhaSanXuatController.getNhaSanXuat()
            .then(resultsNhaSanXuat => {
                res.render('admin/admin-edit-product', {
                    title: 'Edit product',
                    layout: 'admin/admin-layout',
                    data: results[0],
                    dataNSX: resultsNhaSanXuat,
                    dataLSP: resultsLoaiSanPham,
                    flashMessage: req.session.flashMessage
                })
                delete req.session.flashMessage;
                //res.status(200).send(results[0]);
            });
        })
    })
    .catch(error => {
        res.status(500).send(error);
        console.log(error);
    });
});
router.post('/edit/:id', (req, res, next) => {
    const condition = {
        where: {
            id: req.params.id
        }
    };
    productController.updateProducts(condition, req.body)
    .then(result => {
        req.session.flashMessage = 'Update product successfully';
        return res.redirect('back');
    })
    .catch(error => {
        console.log(error);
    });
});
router.get('/add', (req, res, next) => {
    loaiSanPhamController.getLoaiSanPham()
    .then(resultsLoaiSanPham => {
        nhaSanXuatController.getNhaSanXuat()
        .then(resultsNhaSanXuat => {
            res.render('admin/admin-add-product', {
                layout: 'admin/admin-layout',
                title: 'Add product',
                dataNSX: resultsNhaSanXuat,
                dataLSP: resultsLoaiSanPham,
                flashMessage: req.session.flashMessage
            });
            delete req.session.flashMessage;
        });
    })
    .catch(error => {
        res.status(500).send(error);
        console.log(error);
    });
});
router.post('/add', upload.single('hinh_anh'), (req, res, next) => {
    req.body.hinh_anh = BASE_URL_IMG.concat(req.file.filename);
    req.body.so_luot_xem = 0;
    req.body.so_luong_ban = 0;
    req.body.ngay_nhap = moment(new Date()).format('YYYY-MM-DD HH:mm:ssZ');
    productController.createProduct(req.body)
    .then(result => {
        req.session.flashMessage = 'Create product successfully';
        return res.redirect('/admin/products/add');
    })
    .catch(error => {
        return res.status(500).send(error.toString());
    })
});
router.get('/', (req, res, next) => {
    productController.getProductsFollowingCondition()
    .then(results => {
        res.render('admin/admin-products', {
            title: results.length + ' products',
            layout: 'admin/admin-layout',
            data: results,
            flashMessage: req.session.flashMessage
        })
        delete req.session.flashMessage;
    })
    .catch(error => res.status(500).send(error.toString()));
});
export default router;
