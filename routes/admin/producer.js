'use strict';
import express from 'express';
import { productController, nhaSanXuatController } from '../../controllers';

let router = express.Router();

router.get('/', (req, res, next) => {
    nhaSanXuatController.getNhaSanXuat()
    .then(data => {
        return res.render('admin/admin-producer', {
                    title: 'Producers',
                    layout: 'admin/admin-layout',
                    data: data
                })
    })
    .catch(error => {
        console.log(error);
        res.status(500).send({error: error});
    });
});
router.post('/', (req, res, next) => {
        for (let i = 0; i < req.body.nhasx.length; i++) {
            nhaSanXuatController.upsertData({
                id: req.body.id[i],
                nha_san_xuat: req.body.nhasx[i]
            })
            .then(result => {

            })
            .catch(error => {
                return res.status(500).send(error);
            });
        }
        return res.render('admin/admin-producer', {
            title: 'Producers',
            layout: 'admin/admin-layout',
            flashMessage: 'Data updated successfully'
        });
});
export default router;
