'use strict';
import express from 'express';
import { productController, loaiSanPhamController } from '../../controllers';

let router = express.Router();

router.get('/', (req, res, next) => {
    loaiSanPhamController.getLoaiSanPham()
    .then(data => {
        return res.render('admin/admin-assortment', {
                    title: 'Assortments',
                    layout: 'admin/admin-layout',
                    data: data
                })
    })
    .catch(error => {
        console.log(error);
        res.status(500).send({error: error});
    });
});
router.post('/', (req, res, next) => {
        for (let i = 0; i < req.body.loai.length; i++) {
            loaiSanPhamController.upsertData({
                id: req.body.id[i],
                loai: req.body.loai[i]
            })
            .then(result => {

            })
            .catch(error => {
                return res.status(500).send(error);
            });
        }
        return res.render('admin/admin-assortment', {
            title: 'Assortments',
            layout: 'admin/admin-layout',
            flashMessage: 'Data updated successfully'
        });
});
export default router;
