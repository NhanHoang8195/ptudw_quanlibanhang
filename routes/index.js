import express from 'express';
import { productController } from '../controllers';

import { NGAY_NHAP, SO_LUONG_BAN, SO_LUOT_XEM } from '../constants';

const router = express.Router();

router.get('/', (req, res, next) => {
        const additionalCondition1 = {   // lay 10 san pham moi nhat
            order: [[ NGAY_NHAP, 'DESC']],
            limit: 10
        };
        const additionalCondition2 = {
            order: [[ SO_LUONG_BAN, 'DESC']],
            limit: 10
        };
        const additionalCondition3 = {
            order: [[ SO_LUOT_XEM, 'DESC']],
            limit: 10
        };
        productController.getProductsFollowingCondition({}, additionalCondition1)
        .then(resultNewest => {
            productController.getProductsFollowingCondition({}, additionalCondition2)
            .then(resultBestSeller => {   // top 10 san pham ban chay nhat
                productController.getProductsFollowingCondition({}, additionalCondition3)   // top xem nhieu nhat
                .then(resultView => {
                    res.render('index', {
                        title: 'Homepage',
                        username: req.user ? req.user.username : null,
                        data: {
                            top_new: resultNewest,
                            top_sell: resultBestSeller,
                            top_view: resultView
                        }
                    });
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.send(500, {data: error})
        });
});

export default router;
