import express from 'express';
import passport from 'passport';
const router = express.Router();

router.get('/', (req, res, next) => {
    req.session.destroy(function(err) {
        res.redirect('/');
    });
});
export default router;
