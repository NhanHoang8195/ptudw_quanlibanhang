import express from 'express';
const router = express.Router();
import moment from 'moment';
const { or, gt } = require('sequelize').Op;
import lodash from 'lodash';
import Cart from '../constants/shopping-cart-session';
import { productController, accountController, billingController } from '../controllers';
import { CHUA_GIAO_HANG } from '../constants';

router.get('/', (req, res, next) => {
    if(!req.session.cart || req.session.cart.quantity === 0) {
        return res.render('cart', {
            title: 'No production',
            data: null,
            total: null
        });
    }
    const dataCart = req.session.cart.items;
    const id = Object.keys(dataCart);
    const condition = {
        id: {
            [or]: id
        }
    }
    productController.getProductsFollowingCondition(condition)
    .then(results => {
        if(results.length > 0) {
            let total = 0;
            let flashmessage = [];
            results.map(result => {
                if (result.so_luong < dataCart[result.id]) {
                    result.quantity = result.so_luong;
                } else {
                    result.quantity = dataCart[result.id];
                }
                result.totalPrice = result.gia_ban * result.quantity;
                total += result.totalPrice;
            })
            return res.render('cart', {
                title: 'Cart',
                data: results,
                total: total
            });
        } else {
            return res.render('cart');
        }
    })
    .catch(error => {
        res.send(error);
    })
});

router.get('/add/:id', (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.redirect('/login');
    }
    let cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.add(req.params.id);
    req.session.cart = cart;
    return res.redirect('/');
});

router.get('/remove/:id', (req, res, next) => {
    let cart = new Cart(req.session.cart ? req.session.cart : {});
    cart.remove(req.params.id);
    req.session.cart = cart;
    return res.redirect('/cart');
});

router.get('/checkout', (req, res, next) => {
    if(!req.session.cart || req.session.cart.quantity === 0) {
        return res.render('checkout', {
            title: 'No production',
            userdata: null,
            total: null
        });
    }
    if (!req.user.first_name || !req.user.last_name) {
        req.session.previousUrl = '/cart/checkout';
        return res.redirect('/account/profiles');
    }
    const dataCart = req.session.cart.items;
    const id = Object.keys(dataCart);
    const condition = {
        id: {
            [or]: id
        }
    };
    productController.getProductsFollowingCondition(condition)
    .then(results => { // get products from ids
        const dataIgnore = [];
        let total = 0;
        results.map(result => {
            if (result.so_luong < dataCart[result.id]) {
                dataIgnore.push(result);
                if (result.so_luong === 0) {
                    delete req.session.cart.items[result.id];
                    result.quantity = 0;
                } else {
                    req.session.cart.items[result.id] = result.quantity;
                    result.quantity = result.so_luong;
                }
            } else {
                result.quantity = dataCart[result.id]
            }
            result.totalPrice = result.gia_ban * result.quantity;
            total += result.totalPrice;
        })
        req.session.cart.dataBought = results;
        return res.render('checkout', {
            title: 'Checkout',
            userdata: req.user,
            total: total,
            data:results,
            dataIgnore: dataIgnore
        });
    })
    .catch(error => {
        return res.send(error);
    });

});

router.post('/checkout', (req, res, next) => {
    const dataCart = req.session.cart.items;
    const subQuantity = req.session.cart.dataBought;
    const data = {
        delivery_to: req.body.delivery_to,
        ngay_dat_hang: moment(new Date()).format('YYYY-MM-DD HH:mm:ssZ'),
        data: JSON.stringify(dataCart),
        total: req.body.total,
        status: CHUA_GIAO_HANG,
        account_id: req.user.username
    }
    const ids = Object.keys(dataCart);
    billingController.create(data)
    .then(results => {
        if (results) {
            for (let i = 0; i < ids.length; i++) {
                const elementUpdate = subQuantity.find(element => element.id == ids[i]);
                const dataToUpdate = {
                    so_luong: elementUpdate.so_luong - dataCart[ids[i]],
                    so_luong_ban: elementUpdate.so_luong_ban + dataCart[ids[i]]
                }
                const condition = {
                    where: {
                        id: ids[i]
                    }
                }
                productController.updateProducts(condition,  dataToUpdate)
                .then(result => {
                    if (!result) {
                        return res.send({message: 'Can not update products'});
                    }
                })
                .catch(error => {
                    return res.send(error);
                });
            }
            delete req.session.cart;
            return res.render('checkout', {
                title: 'Checkout',
                flashmessage: 'Buy products successful!!!'
            });
        } else {
            return res.send(result);
        }
    })
    .catch(error => {
        return res.send(error);
    });
});
export default router;
