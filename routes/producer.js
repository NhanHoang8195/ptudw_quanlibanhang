import express from 'express';
import { nhaSanXuatController, productController } from '../controllers';
const router = express.Router();

router.get('/', (req, res, next) => {
    if (req.query.typeproducts) {
        productController.getProductsFollowingCondition({nha_san_xuat: req.query.typeproducts})
        .then(results => {
            return res.render('products', {
                    title: 'Nhà sản xuất',
                    data: results
                });
        })
        .catch(error => {
            return res.send(error);
        });
    } else {
        nhaSanXuatController.getNhaSanXuat()
        .then(results => {
            return res.render('producer', {
                title: 'Danh sách các nhà sản xuất',
                data: results,
            });
        })
        .catch(error => {
            console.log(error);
            res.status(500).send({error: error});
        });
    }
});

export default router;
