--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE "SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE "SequelizeMeta" OWNER TO postgres;

--
-- Name: account; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE account (
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    age integer,
    address character varying(255),
    admin boolean
);


ALTER TABLE account OWNER TO postgres;

--
-- Name: lich_su_mua_hang; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lich_su_mua_hang (
    id integer NOT NULL,
    ngay_dat_hang timestamp with time zone,
    total character varying(255),
    data character varying(255),
    delivery_to character varying(255),
    status character varying(255),
    account_id character varying(255) NOT NULL
);


ALTER TABLE lich_su_mua_hang OWNER TO postgres;

--
-- Name: lich_su_mua_hang_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE lich_su_mua_hang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE lich_su_mua_hang_id_seq OWNER TO postgres;

--
-- Name: lich_su_mua_hang_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE lich_su_mua_hang_id_seq OWNED BY lich_su_mua_hang.id;


--
-- Name: loai_san_pham; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE loai_san_pham (
    id integer NOT NULL,
    loai character varying(255) NOT NULL
);


ALTER TABLE loai_san_pham OWNER TO postgres;

--
-- Name: loai_san_pham_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE loai_san_pham_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE loai_san_pham_id_seq OWNER TO postgres;

--
-- Name: loai_san_pham_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE loai_san_pham_id_seq OWNED BY loai_san_pham.id;


--
-- Name: nha_san_xuat; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nha_san_xuat (
    id integer NOT NULL,
    nha_san_xuat character varying(255) NOT NULL
);


ALTER TABLE nha_san_xuat OWNER TO postgres;

--
-- Name: nha_san_xuat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE nha_san_xuat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nha_san_xuat_id_seq OWNER TO postgres;

--
-- Name: nha_san_xuat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE nha_san_xuat_id_seq OWNED BY nha_san_xuat.id;


--
-- Name: san_pham; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE san_pham (
    id integer NOT NULL,
    ten_san_pham character varying(255),
    hinh_anh character varying(255),
    gia_ban integer,
    so_luot_xem integer,
    so_luong_ban integer,
    mo_ta character varying(255),
    xuat_xu character varying(255),
    loai character varying(255) NOT NULL,
    ngay_nhap timestamp with time zone,
    nha_san_xuat character varying(255) NOT NULL,
    so_luong integer
);


ALTER TABLE san_pham OWNER TO postgres;

--
-- Name: san_pham_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE san_pham_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE san_pham_id_seq OWNER TO postgres;

--
-- Name: san_pham_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE san_pham_id_seq OWNED BY san_pham.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lich_su_mua_hang ALTER COLUMN id SET DEFAULT nextval('lich_su_mua_hang_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY loai_san_pham ALTER COLUMN id SET DEFAULT nextval('loai_san_pham_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY nha_san_xuat ALTER COLUMN id SET DEFAULT nextval('nha_san_xuat_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY san_pham ALTER COLUMN id SET DEFAULT nextval('san_pham_id_seq'::regclass);


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY "SequelizeMeta" (name) FROM stdin;
20180416133205-create-account.js
20180416135053-create-lich-su-mua-hang.js
20180517064643-create-loai-san-pham.js
20180517064709-create-nha-san-xuat.js
20180518064642-create-san-pham.js
\.


--
-- Data for Name: account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY account (username, password, first_name, last_name, age, address, admin) FROM stdin;
abc@gmail.com	$2a$10$fLmV2qp2NFf6SjGgu5uXoeSI9cijGjL2HH4XslZgprdoeZr3LJoV2	\N	\N	\N	\N	f
\.


--
-- Data for Name: lich_su_mua_hang; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY lich_su_mua_hang (id, ngay_dat_hang, total, data, delivery_to, status, account_id) FROM stdin;
\.


--
-- Name: lich_su_mua_hang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('lich_su_mua_hang_id_seq', 1, false);


--
-- Data for Name: loai_san_pham; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY loai_san_pham (id, loai) FROM stdin;
1	may anh ong kinh roi
2	may anh du lich
3	cam bien may anh so
4	may anh ong kinh lien
\.


--
-- Name: loai_san_pham_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('loai_san_pham_id_seq', 4, true);


--
-- Data for Name: nha_san_xuat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY nha_san_xuat (id, nha_san_xuat) FROM stdin;
1	Sony
2	Canon
3	Nikon
4	Fujifilm
\.


--
-- Name: nha_san_xuat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('nha_san_xuat_id_seq', 4, true);


--
-- Data for Name: san_pham; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY san_pham (id, ten_san_pham, hinh_anh, gia_ban, so_luot_xem, so_luong_ban, mo_ta, xuat_xu, loai, ngay_nhap, nha_san_xuat, so_luong) FROM stdin;
1	Nikon D3300	http://localhost:8080/images/mayanhso/nikon/2319317_Nikon_D3300_ong_18-55_moi_500px.jpg	1	1	1	May anh so dep va sac net Nikon	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Nikon	50
2	Nikon D5600	http://localhost:8080/images/mayanhso/nikon/d5600.jpg	2	2	2	May anh so dep va sac net Nikon	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Nikon	50
3	Nikon D7100	http://localhost:8080/images/mayanhso/nikon/D7100.jpg	3	3	3	May anh so dep va sac net Nikon	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Nikon	50
4	Nikon D750	http://localhost:8080/images/mayanhso/nikon/may-anh-Nikon-D750.jpg	4	4	4	May anh so dep va sac net Nikon	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Nikon	50
5	Nikon D750	http://localhost:8080/images/mayanhso/nikon/may-anh-nikon-d750-lens-kit-24-120mm.jpeg	5	5	5	May anh so dep va sac net Nikon	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Nikon	50
6	Nikon D810	http://localhost:8080/images/mayanhso/nikon/may-anh-Nikon-D810.jpg	6	6	6	May anh so dep va sac net Nikon	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Nikon	50
7	Nikon D7100	http://localhost:8080/images/mayanhso/nikon/may-anh-nikon-d7100-lens-kit-18-140mm.jpeg	7	7	7	May anh so dep va sac net Nikon	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Nikon	50
8	Nikon D7200	http://localhost:8080/images/mayanhso/nikon/Nikon-D7200.jpg	7	7	7	May anh so dep va sac net Nikon	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Nikon	50
9	Nikon D3400	http://localhost:8080/images/mayanhso/nikon/Nikon_D3400.jpg	8	8	8	May anh so dep va sac net Nikon	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Nikon	50
10	Nikon J1	http://localhost:8080/images/mayanhso/nikon/Nikon_J1_1.jpg	9	9	9	May anh so dep va sac net Nikon	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Nikon	50
11	Nikon D810	http://localhost:8080/images/mayanhso/nikon/may-anh-Nikon-D810.jpg	10	10	10	May anh so dep va sac net Nikon	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Nikon	50
12	Canon ixus 180	http://localhost:8080/images/mayanhso/canon/3761_canon_ixus_180_2.jpg	11	11	11	May anh so dep va sac net Canon	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Canon	50
13	Canon-EOS-6D	http://localhost:8080/images/mayanhso/canon/Canon-EOS-6D.jpg	12	12	12	May anh so dep va sac net Canon	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Canon	50
14	Canon-isx-80	http://localhost:8080/images/mayanhso/canon/canon-ixs-80.jpg	13	13	13	May anh so dep va sac net Canon	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Canon	50
15	Canon-IXUS-285	http://localhost:8080/images/mayanhso/canon/Canon-IXUS-285-hs3-jpg.jpg	14	14	14	May anh so dep va sac net Canon	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Canon	50
16	Canon-ELPH-190	http://localhost:8080/images/mayanhso/canon/ELPH-190.jpg	15	15	15	May anh so dep va sac net Canon	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Canon	50
17	Canon-ixus-170	http://localhost:8080/images/mayanhso/canon/may-anh-canon-ixus-170-den.jpg	16	16	16	May anh so dep va sac net Canon	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Canon	50
18	Canon-ixus-175	http://localhost:8080/images/mayanhso/canon/may-anh-canon-ixus-175-bac-6zuSe9.jpg	17	17	17	May anh so dep va sac net Canon	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Canon	50
19	Canon-ixus-275hs	http://localhost:8080/images/mayanhso/canon/may-anh-canon-ixus-275hs-bac-2.jpg	18	18	18	May anh so dep va sac net Canon	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Canon	50
20	Canon-ixus-275hs-bac	http://localhost:8080/images/mayanhso/canon/may-anh-canon-ixus-275hs-bac-3.jpg	19	19	19	May anh so dep va sac net Canon	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Canon	50
21	Canon-ixus-275hs-bac	http://localhost:8080/images/mayanhso/canon/may-anh-canon-ixus-275hs-bac-6.jpg	20	20	20	May anh so dep va sac net Canon	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Canon	50
22	Fujifilm S1600	http://localhost:8080/images/mayanhso/fujifilm/fujifilmS1600.jpg	21	21	21	May anh so dep va sac net Fujifilm	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Fujifilm	50
23	Fujifilm X100s	http://localhost:8080/images/mayanhso/fujifilm/fujifilm-x100s-va-su-tro-lai-cua-thu-choi-may-anh-fuji-0.jpg	22	22	22	May anh so dep va sac net Fujifilm	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Fujifilm	50
24	Fujifilm E1	http://localhost:8080/images/mayanhso/fujifilm/jujifilmX-E1.jpg	23	23	23	May anh so dep va sac net Fujifilm	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Fujifilm	50
25	Fujifilm S4300	http://localhost:8080/images/mayanhso/fujifilm/may-anh-fujifilm-s4300-8.jpg	24	24	24	May anh so dep va sac net Fujifilm	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Fujifilm	50
26	Fujifilm X-A3	http://localhost:8080/images/mayanhso/fujifilm/may-anh-fujifilm-x-a3-lens-kit-16-50mm.jpeg	25	25	25	May anh so dep va sac net Fujifilm	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Fujifilm	50
27	Fujifilm X-E2	http://localhost:8080/images/mayanhso/fujifilm/may-anh-fujifilm-x-e2-xf18-55-s-ee-bac-1.jpg	26	26	26	May anh so dep va sac net Fujifilm	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Fujifilm	50
28	Fujifilm X-E3	http://localhost:8080/images/mayanhso/fujifilm/May-Anh-Fujifilm-X-E3-Kit-23mm-f2-R-WR--Bac--jpg.jpg	27	27	27	May anh so dep va sac net Fujifilm	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Fujifilm	50
29	Fujifilm X-T2	http://localhost:8080/images/mayanhso/fujifilm/MAY-ANH-FUJIFILM-X-T2-2-1.jpg	28	28	28	May anh so dep va sac net Fujifilm	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Fujifilm	50
30	Fujifilm S9400w	http://localhost:8080/images/mayanhso/fujifilm/may-anh-ki-thuat-so-fujifilm-s9400w-1.jpg	29	29	29	May anh so dep va sac net Fujifilm	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Fujifilm	50
31	Fujifilm X-T20	http://localhost:8080/images/mayanhso/fujifilm/x-t20---lens-16-50mm-bac-1.u4064.d20170512.t155950.140337_2.jpg	30	30	30	May anh so dep va sac net Fujifilm	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Fujifilm	50
32	Sony W830	http://localhost:8080/images/mayanhso/sony/15129_2_may_anh_cyber_shot_digital_camera_w830_black_003.jpg	31	31	31	May anh so dep va sac net Sony	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Sony	50
33	Sony ilce-6000l	http://localhost:8080/images/mayanhso/sony/15706_1_may_anh_ky_thuat_so_sony_ilce_6000l_black___002.jpg	32	32	32	May anh so dep va sac net Sony	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Sony	50
34	Sony KTS-WX220	http://localhost:8080/images/mayanhso/sony/0305927-may-anh-KTS-SONY-DSC-WX220-mau-hong-01.jpg	33	33	33	May anh so dep va sac net Sony	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Sony	50
35	Sony W830	http://localhost:8080/images/mayanhso/sony/may-anh-sony-cybershot-dsc-w830-chinh-hang-bao-hanh-2-nam-1m4G3-co4WYc_simg_d0daf0_800x1200_max.jpg	34	34	34	May anh so dep va sac net Sony	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Sony	50
36	Sony W810	http://localhost:8080/images/mayanhso/sony/may-anh-sony-dsc-w810.jpg	35	35	35	May anh so dep va sac net Sony	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Sony	50
37	Sony Alpha A7SII	http://localhost:8080/images/mayanhso/sony/Sony-Alpha-A7SII.jpg	36	36	36	May anh so dep va sac net Sony	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Sony	50
38	Sony H300-Black	http://localhost:8080/images/mayanhso/sony/SonyCyberShotH300-Black.jfif	37	37	37	May anh so dep va sac net Sony	Nhat ban	may anh ong kinh lien	2018-05-19 14:44:37+07	Sony	50
39	Sony A7	http://localhost:8080/images/mayanhso/sony/Sony-A7.jpg	38	38	38	May anh so dep va sac net Sony	Nhat ban	cam bien may anh so	2018-05-19 14:44:37+07	Sony	50
40	Sony A5100	http://localhost:8080/images/mayanhso/sony/sony-A5100.jpg	39	39	39	May anh so dep va sac net Sony	Nhat ban	may anh du lich	2018-05-19 14:44:37+07	Sony	50
41	Sony W810BCE3	http://localhost:8080/images/mayanhso/sony/W810BCE3.png	40	40	40	May anh so dep va sac net Sony	Nhat ban	may anh ong kinh roi	2018-05-19 14:44:37+07	Sony	50
\.


--
-- Name: san_pham_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('san_pham_id_seq', 41, true);


--
-- Name: SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey PRIMARY KEY (username);


--
-- Name: lich_su_mua_hang_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lich_su_mua_hang
    ADD CONSTRAINT lich_su_mua_hang_pkey PRIMARY KEY (id);


--
-- Name: loai_san_pham_loai_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY loai_san_pham
    ADD CONSTRAINT loai_san_pham_loai_key UNIQUE (loai);


--
-- Name: loai_san_pham_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY loai_san_pham
    ADD CONSTRAINT loai_san_pham_pkey PRIMARY KEY (id);


--
-- Name: nha_san_xuat_nha_san_xuat_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nha_san_xuat
    ADD CONSTRAINT nha_san_xuat_nha_san_xuat_key UNIQUE (nha_san_xuat);


--
-- Name: nha_san_xuat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nha_san_xuat
    ADD CONSTRAINT nha_san_xuat_pkey PRIMARY KEY (id);


--
-- Name: san_pham_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY san_pham
    ADD CONSTRAINT san_pham_pkey PRIMARY KEY (id);


--
-- Name: lich_su_mua_hang_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lich_su_mua_hang
    ADD CONSTRAINT lich_su_mua_hang_account_id_fkey FOREIGN KEY (account_id) REFERENCES account(username) ON DELETE CASCADE;


--
-- Name: san_pham_loai_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY san_pham
    ADD CONSTRAINT san_pham_loai_fkey FOREIGN KEY (loai) REFERENCES loai_san_pham(loai) ON UPDATE CASCADE;


--
-- Name: san_pham_nha_san_xuat_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY san_pham
    ADD CONSTRAINT san_pham_nha_san_xuat_fkey FOREIGN KEY (nha_san_xuat) REFERENCES nha_san_xuat(nha_san_xuat) ON UPDATE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

